
cd `dirname $0`
echo '-- start watching'

coffee -o page/ -wbc src/*coffee &
jade -O page/ -wP src/*jade &
stylus -o page -w src/*styl &
doodle page/ &

read

pkill -f jade
pkill -f stylus
pkill -f coffee
pkill -f doodle

echo '-- stop watching'