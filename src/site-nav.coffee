
site_nav = ->

  query = (str) -> document.querySelector str
  show = (obj) -> console.log obj

  data =
    "精弘": "http://www.zjut.com/"
    "BBS": 'http://bbs.zjut.com/'
    "Feel": 'http://feel.zjut.com/'
    "Mirror": 'http://mirror.zjut.com/'
    "Go": 'http://go.zjut.com/'
    "Mail": "http://mail.zjut.com/index.php"
    "Down": "http://down.zjut.com/"
    "PT": "http://pt.zjut.com/login.php"
    "Apps": 'http://s.zjut.in/app-nav/page/'

  table = ->
    str = ""

    for key, value of data
      str += "<p><a href='#{value}' target='_blank'>#{key}</a></p>"

    str

  nav = document.createElement 'div'
  nav.setAttribute 'id', 'site-nav'
  nav.innerHTML = table()
  document.body.appendChild nav

window.onload = site_nav